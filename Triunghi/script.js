function verifyTriangle(){
  var ab = parseInt(document.getElementById("latAB").value);
  var ac = parseInt(document.getElementById("latAC").value);
  var bc = parseInt(document.getElementById("latBC").value);

  if (checkValidInputs(ab, ac, bc)){
    if (checkIfIsTriangle(ab,ac,bc)) {
      document.getElementById("inputTriangle").innerHTML = "da";
      document.getElementById("inputArea").innerHTML = String(getArea(ab,ac,bc));
      document.getElementById("inputPerimeter").innerHTML = String(getPerimeter(ab,ac,bc));
      document.getElementById("inputCosA").innerHTML = String(getCos(bc,ac,ab));
      document.getElementById("inputCosB").innerHTML = String(getCos(ac,ab,bc));
      document.getElementById("inputCosC").innerHTML = String(getCos(ab,ac,bc));
    } else {
      document.getElementById("inputTriangle").innerHTML = "nu";
    }

  }else{
    document.getElementById("inputTriangle").innerHTML = "error";
  }
}

function checkIfIsTriangle(ab,ac,bc) {
    if (ac + bc >= ab) {
      return true;
    } else {
      return false;
    }
}

function getArea(ab, ac, bc){
    let semiPeremeter= (ab+ac+bc)/2;
    return Math.sqrt(semiPeremeter*(semiPeremeter-ab)*(semiPeremeter-ac)*(semiPeremeter-bc))
}

function getPerimeter(ab,ac,bc){
  return (ab+ac+bc)/3;
}

function getCos(x,y,z){
    return (y*y + z*z - x*x)/(2*y*z)
}

function checkValidInputs(ab, ac, bc) { 
  console.log(ab)
  if (ab && ac && bc ) {
    return true;
  }
  return false;
}
